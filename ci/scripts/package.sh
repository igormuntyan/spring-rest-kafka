#!/bin/sh

cd resource-test

export TERM=${TERM:-dumb}

gradle -Dorg.gradle.native=false build

ls build/libs

cp build/libs/test.jar ../resource-jar

cp Dockerfile ../resource-jar
